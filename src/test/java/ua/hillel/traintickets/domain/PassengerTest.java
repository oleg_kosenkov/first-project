package ua.hillel.traintickets.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class PassengerTest {

    private final Long   ID = 10L;
    private final String NAME = "Ivan Koval";
    private final String EMAIL = "koval@gmail.com";
    private final String PASSWORD = "password";
    private Passenger passenger;

    @Before
    public void setup() {
        passenger = new Passenger();
    }

    @Test
    public void setId() {
        passenger.setId(ID);
        assertEquals(ID, passenger.getId());
    }

    @Test
    public void setName() {
        passenger.setName(NAME);
        assertEquals(NAME, passenger.getName());
    }

    @Test
    public void setEmail() {
        passenger.setEmail(EMAIL);
        assertEquals(EMAIL, passenger.getEmail());
    }

    @Test
    public void setPassword() {
        passenger.setPassword(PASSWORD);
        assertEquals(PASSWORD, passenger.getPassword());
    }
}