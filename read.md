Tutorial project to create a repository from scratch.

1) Create in idea a maven project locally.
   Edit pom.xml if needed.

2) Create read.md and .gitignore

3) from the terminal window in idea, under the project path, run
  $ git init.

4) Run
   $ git add pom. xml read.md .gitignore
5) $ git commit pom. xml read.md .gitignore -m"init"
6) Create a repo on bitbucket with the same name as the project
7) Run commands, that bitbucket suggests to push local repo to origin:
  $ git remote add origin https://oleg_kosenkov@bitbucket.org/oleg_kosenkov/first-project.git
  $ git push -u origin master
  in the pop-up window, provide your password to gitbucket

The magic happend!

To https://bitbucket.org/oleg_kosenkov/first-project.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.

8) Check out new branch
  $  git checkout -b feature/MyClass

9) With idea create new class MyClass under package ua.hillel.traintickets.domain

10) Add fields, setters, getters, equals, hashcode

11) Create test class, add setter-getter tests, run them.

12) $ git add src/main/java/ua/hillel/traintickets/domain/MyClass.java

13) $ git add src/test/java/ua/hillel/traintickets/domain/MyClassTest.java

14) $ git commit pom.xml read.md src/main/java/ua/hillel/traintickets/domain/MyClass.java src/test/java/ua/hillel/traintickets/domain/MyClassTest.java -m"MyClass created"

15) Push the branch to bitbucket
  $ git push -u origin feature/MyClass
